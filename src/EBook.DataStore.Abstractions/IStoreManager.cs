﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Abstractions
{
  public  interface IStoreManager
    {
        bool IsInitialized { get; }
        Task InitializeAsync();

        ISubjectStore SubjectStore { get; }
        IQuestionStore QuestionStore { get; }

        Task<bool> SyncAllAsync(bool syncUserSpecific);

        Task PurgeAllAsync();
        Task DropEverythingAsync();
        void ResetTables();
    }
}
