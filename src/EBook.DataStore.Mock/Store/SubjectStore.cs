﻿using EBook.DataStore.Abstractions;
using EBook.DataStore.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Mock
{
   public class SubjectStore : BaseStore<Subject>, ISubjectStore
    {

        public List<Subject> Subjects = new List<Subject>();

        public SubjectStore()
        {
            SetUpSubjects();
        }

        public async override Task<Subject> GetItemAsync(string id, bool forceRefresh = false)
        {
            Subject retVal = new Subject();
            retVal = Subjects.FirstOrDefault(i => i.Id == id);
            return await Task.FromResult(retVal);
        }


        public async override Task<IEnumerable<Subject>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(Subjects.AsEnumerable());
        }

        private void SetUpSubjects()
        {
            Subjects = new List<Subject>()
            {
                new Subject() {Id ="1", Name="English Language"},
                new Subject() {Id ="2", Name="Mathematics" }
            };
        }
    }
}
