﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using EBook.Core;
using EBook.Abstractions;
using FreshMvvm;

namespace EBook.Droid
{
    [Activity(Label = "EBook", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            Bootstrap_Init();

            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }

        private void SetupMrGestures()
        {
            MR.Gestures.Android.Settings.LicenseKey = EnvironmentConfiguration.Instance.MrGesturesKey;

        }

        void Bootstrap_Init()
        {
            //SetupHockeyApp();

            //needs to be called before registering services on android because of acr dialog
            InitComponents();

           RegisterServices();
        }

        private void InitComponents()
        {
            Acr.UserDialogs.UserDialogs.Init(this);
           // SvgImageRenderer.InitializeForms();
        }

        private void RegisterServices()
        {
            FreshIOC.Container.Register<ITextToSpeech, TextToSpeechImplementation>();
           FreshIOC.Container.Register<IAudio, AudioService>();
        }
    }
}

