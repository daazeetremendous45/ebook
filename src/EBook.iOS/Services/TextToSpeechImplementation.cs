using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AVFoundation;
using EBook.Abstractions;
using EBook.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(TextToSpeechImplementation))]
namespace EBook.iOS
{
    class TextToSpeechImplementation : ITextToSpeech
    {
        public TextToSpeechImplementation() { }

        public void Speak(string text)
        {
            var speechSynthesizer = new AVSpeechSynthesizer();

            var speechUtterance = new AVSpeechUtterance(text)
            {
                Rate = AVSpeechUtterance.MaximumSpeechRate / 4,
                Voice = AVSpeechSynthesisVoice.FromLanguage("en-US"),
                Volume = 0.5f,
                PitchMultiplier = 1.0f
            };

            speechSynthesizer.SpeakUtterance(speechUtterance);
        }
    }
}