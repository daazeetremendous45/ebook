﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.Abstractions
{
   public interface ISession
    {
        AuthenticationResponse CurrentUser { get; set; }
    }
}
