﻿using Acr.UserDialogs;
using EBook.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EBook.Core
{
    public class ErrorManager : IErrorManager
    {
        readonly IUserDialogs Dialog;

        public ErrorManager(IUserDialogs dialog)
        {
            Dialog = dialog;

        }

        public async Task DisplayErrorMessageAsync(Exception ex, string errorMessage = null)
        {
            //log exception
            LogException(ex);


            //handle general network errors
            if (ex.Message.ToLower().Contains("nsurl"))
            {
                await Dialog.AlertAsync("We are unable to connect at this time. Please check your network connection.", "Error");


            }
            else
            {
                if (String.IsNullOrEmpty(errorMessage))
                {
                    await Dialog.AlertAsync(ex.Message, "Error");

                }
                else
                {
                    await Dialog.AlertAsync(errorMessage, "Error");
                }

            }

        }

        public void LogException(Exception ex, bool rethrow = false, [CallerMemberName] string caller = "")
        {
            //log exception

            //Logger.Report(ex);

            if (rethrow)
            {
                throw ex;
            }
        }
    }
}
