﻿using EBook.Core.Pages.Question;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EBook.Core.Pages._Common.ComingSoon
{
  public class ComingSoonViewModel : BaseViewModel
    {

        Command _NextCommand;
        public const string NextCommandPropertyName = "_NextCommand";

        public Command NextCommand
        {
            get
            {
                return _NextCommand ?? (_NextCommand = new Command<string>(async (obj) => await Next()));
            }
        }

        protected async Task Next()
        {
            
                    await CoreMethods.PushPageModel<QuestionViewModel>();
          
        }


    }
}
