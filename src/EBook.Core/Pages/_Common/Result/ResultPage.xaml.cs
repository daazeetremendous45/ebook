﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace EBook.Core
{
    public partial class ResultPage : ContentPage
    {
        public ResultPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
