﻿using Acr.UserDialogs;
using EBook.Abstractions;
using EBook.Core.Pages.End;
using EBook.DataStore.Abstractions;
using FreshMvvm;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EBook.Core.Pages.Question
{
    [ImplementPropertyChanged]
    public class QuestionViewModel : BaseViewModel
    {
        readonly IApp App;
        readonly IErrorManager ErrorManager;
        readonly IUserDialogs Dialog;
        readonly IAppSettings AppSettings;
        readonly IQuestionStore QuestionStore;
        ITextToSpeech textToSpeeech = FreshIOC.Container.Resolve<ITextToSpeech>();
        IAudio AudioPlayer = FreshIOC.Container.Resolve<IAudio>();
        public DataStore.DataObjects.Question Question { get; set; }
        public string Answer { get; set; }
        public bool IsOptionEnabled { get; set; } = true;

        public int Sn { get; set; }
        public QuestionViewModel(IApp app, IErrorManager errorManager, IUserDialogs userDialogs,
                                IAppSettings appSettings, IQuestionStore questionStore)
        {
            App = app;
            ErrorManager = errorManager;
            Dialog = userDialogs;
            AppSettings = appSettings;
            QuestionStore = questionStore;
        }

        public async override void Init(object initData)
        {
            base.Init(initData);
            //Data = initData as Nav;
            Sn = 1;//First Question
            GetNextQuestion(AppSettings.CurrentSubjectId, Sn);
        }

        public async override void ReverseInit(object returnedData)
        {
            try
            {
                var data = returnedData as ResultViewModel.ReturnNav;

                IsOptionEnabled = true;
                await NextQuestion(Sn);
                if (data.IsRight)
                {
                    if (Sn == 10)
                        await CoreMethods.PushPageModel<EndViewModel>();
                    else
                    {
                        Sn += 1;
                        await NextQuestion(Sn);
                    }

                }
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        public async void GetNextQuestion(int subjectID, int sn)
        {
            Question = await QuestionStore.GetNextQuestion(subjectID, sn);
            textToSpeeech.Speak(Question.Text);
        }

        Command _NextQuestionCommand;
        public const string NextQuestionCommandPropertyName = "_NextQuestionCommand";

        public Command NextQuestionCommand
        {
            get
            {
                return _NextQuestionCommand ?? (_NextQuestionCommand = new Command<string>(async (obj) => await NextQuestion(Sn)));
            }
        }

        protected async Task NextQuestion(int sn)
        {
            try
            {
                // Question = await QuestionStore.GetNextQuestion(AppSettings.CurrentSubjectId, Sn);
                //if (sn == 10)
                //    await CoreMethods.PushPageModel<EndViewModel>();
                GetNextQuestion(AppSettings.CurrentSubjectId, sn);

            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        protected async Task SetNextQuestion(string selectedOption)
        {
            if (IsOptionEnabled)
            {
                textToSpeeech.Speak(selectedOption);
                IsOptionEnabled = false;
                if (selectedOption == Question.Answer)
                {
                    AudioPlayer.Success();
                    await Task.Delay(5000);

                    var nav = new ResultViewModel.Nav { IsRight = true };
                    await CoreMethods.PushPageModel<ResultViewModel>(nav, true);
                }
                else
                {
                    AudioPlayer.Failure();
                    await Task.Delay(5000);
                    var nav = new ResultViewModel.Nav { IsRight = false };
                    await CoreMethods.PushPageModel<ResultViewModel>(nav, true);
                }
            }
        }

        #region Command - Option1Command

        private Command _Option1Command;
        public const string Option1CommandPropertyName = "Option1Command";

        public Command Option1Command
        {

            get
            {
                return _Option1Command ?? (_Option1Command = new Command(async () => await ExecuteOption1Command()));
            }

        }

        protected async Task ExecuteOption1Command()
        {
            try
            {

                await SetNextQuestion(Question.Option1);
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        #endregion

        #region Command - Option2Command

        private Command _Option2Command;
        public const string Option2CommandPropertyName = "Option2Command";

        public Command Option2Command
        {
            get
            {
                return _Option2Command ?? (_Option2Command = new Command(async () => await ExecuteOption2Command()));
            }
        }

        protected async Task ExecuteOption2Command()
        {
            try
            {
                await SetNextQuestion(Question.Option2);
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        #endregion

        #region Command - Option3Command

        private Command _Option3Command;
        public const string Option3CommandPropertyName = "Option3Command";

        public Command Option3Command
        {
            get
            {
                return _Option3Command ?? (_Option3Command = new Command(async () => await ExecuteOption3Command()));
            }
        }

        protected async Task ExecuteOption3Command()
        {

            try
            {
                await SetNextQuestion(Question.Option3);
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        #endregion

    }
}
