﻿using System;
using System.Threading.Tasks;
using EBook.Abstractions;
using PropertyChanged;
using Xamarin.Forms;

namespace EBook.Core
{
    [ImplementPropertyChanged]
    public class ResultViewModel : BaseViewModel
    {
        readonly IErrorManager ErrorManager;

        public Color BackgroundColor { get; set; }
        public string Message { get; set; }
        public string SignImage { get; set; }
        public string NextImage { get; set; }

        Nav Data;
        public class Nav
        {
            public bool IsRight { get; set; }
        }

        public class ReturnNav
        {
            public bool IsRight { get; set; }
        }

        public ResultViewModel(IErrorManager errorManager)
        {
            ErrorManager = errorManager;
        }

        public async override void Init(object initData)
        {
            base.Init(initData);

            try
            {
                Data = initData as Nav;

                BackgroundColor = Data.IsRight ? Color.FromHex("#7CAE7A") : Color.FromHex("#BA1B1D");
                Message = Data.IsRight ? "Good Job" : "Try again";
                SignImage = Data.IsRight ? "smile.png" : "red_x.png";
                NextImage = Data.IsRight ? "white_arrow.png" : "green_arrow.png";
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

		#region Command - NextCommand

		private Command _NextCommand;
		public const string NextCommandPropertyName = "NextCommand";

		public Command NextCommand
		{
			get
			{
				return _NextCommand ?? (_NextCommand = new Command(async () => await ExecuteNextCommand()));
			}
		}

		protected async Task ExecuteNextCommand()
		{
			try
			{
                var returnNav = new ReturnNav { IsRight = Data.IsRight };
                await CoreMethods.PopPageModel(returnNav, true);
			}
			catch (Exception ex)
			{
				await ErrorManager.DisplayErrorMessageAsync(ex);
			}
		}

		#endregion
	}
}
