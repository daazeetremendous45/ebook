﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.DataObjects
{
    //[JsonObject(Title = "Question")]
    //[ImplementPropertyChanged]
    public class Question : BaseDataObject
    {
        public int QuestionNo { get; set; }

        public int SubjectID { get; set; }
        public string Text { get; set; }
        public string Option1 { get; set; }

        public string Option2 { get; set; }

        public string Option3 { get; set; }

        public string Answer { get; set; }
    }
}
