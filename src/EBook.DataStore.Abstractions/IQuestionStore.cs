﻿using EBook.DataStore.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Abstractions
{
  public  interface IQuestionStore : IBaseStore<Question>
    {
      Task<Question> GetNextQuestion(int subjectID, int sn);
    }
}
