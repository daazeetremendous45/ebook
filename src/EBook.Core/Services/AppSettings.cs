﻿using EBook.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.Core
{
    public class AppSettings : IAppSettings
    {
        public int CurrentSubjectId { get; set; }
    }
}
