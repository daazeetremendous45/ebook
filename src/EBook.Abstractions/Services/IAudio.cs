﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.Abstractions
{
    public interface IAudio
    {
        void Success();
        void Failure();
    }
}
