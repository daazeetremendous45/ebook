﻿using System;
using EBook.Core;
using EBook.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomFrame), typeof(CustomFrameRenderer))]
namespace EBook.iOS
{
    public class CustomFrameRenderer : VisualElementRenderer<Frame>
    {
        public CustomFrameRenderer()
        {
            Layer.BorderColor = UIColor.Black.CGColor;
			Layer.BorderWidth = 3;
        }
    }
}
