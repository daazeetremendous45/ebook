﻿using EBook.Abstractions;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EBook.Core
{
    [ImplementPropertyChanged]
    public class LoginViewModel : BaseViewModel
    {
        IErrorManager ErrorManager;
        IApp App;

        public LoginViewModel(IErrorManager errorManager, IApp app)
        {
            ErrorManager = errorManager;
            App = app;
        }


        Command _LoginCommand;
        public const string LoginCommadPropertyName = "_LoginCommand";

        public Command LoginCommand
        {
            get
            {
                return _LoginCommand ?? (_LoginCommand = new Command<string>(async (obj) => await ExecuteLoginCommand()));
            }
        }

        protected async Task ExecuteLoginCommand()
        {
            try
            {
                //Nav c = new Nav();
                //c.customerPhoneNumber = "07036135902";
                //App.StartWelcomeFlow();

            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }
    }
}
