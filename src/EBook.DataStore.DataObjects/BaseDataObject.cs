﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.DataObjects
{
    public interface IBaseDataObject
    {
        string Id { get; set; }
    }
    public class BaseDataObject : IBaseDataObject
    {
       // [Newtonsoft.Json.JsonProperty("Id")]
        public string Id { get; set; }

       // [Newtonsoft.Json.JsonProperty("Version")]
        public string Version { get; set; }

       // [Newtonsoft.Json.JsonProperty("CreatedAt")]
        public DateTimeOffset CreatedAt { get; set; }

       // [Newtonsoft.Json.JsonProperty("UpdatedAt")]
        public DateTimeOffset UpdatedAt { get; set; }

        //[Newtonsoft.Json.JsonProperty("Deleted")]
        public bool Deleted { get; set; }

        public BaseDataObject()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
