﻿using System;
using EBook.Core;
using EBook.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomFrame), typeof(CustomFrameRenderer))]
namespace EBook.Droid
{
    public class CustomFrameRenderer : VisualElementRenderer<Frame>
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
		{
			base.OnElementChanged(e);
			this.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.black_rect));
		}
    }
}
