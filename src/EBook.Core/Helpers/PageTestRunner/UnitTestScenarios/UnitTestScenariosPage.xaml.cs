﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace EBook.Core.Helpers.PageTestRunner.UnitTestScenarios
{
    public partial class UnitTestScenariosPage : ContentPage
    {
        public UnitTestScenariosPage()
        {
            InitializeComponent();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            try
            {
                var list = sender as ListView;
                UnitTestsItemViewModel vm = list.SelectedItem as UnitTestsItemViewModel;
                vm.ViewPageCommand.Execute(null);
                list.SelectedItem = null;
            }
            catch (Exception ex)
            {
                //ErrorManager.DisplayErrorMessage(this, ex);

            }

        }

    }
}
