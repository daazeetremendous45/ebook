﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Abstractions
{
 public   interface IBaseStore<T>
    {
        IStoreManager Manager { get; set; }

        Task InitializeStore();
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
        Task<T> GetItemAsync(string id, bool forceRefresh = false);
        Task<bool> InsertAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<bool> RemoveAsync(T item);

        Task<bool> InsertOnlineAsync(T item);
        Task<bool> UpdateOnlineAsync(T item);
        Task<bool> RemoveOnlineAsync(T item);

        Task<bool> SyncAsync(bool pushPendingTransactions = true);
        Task<bool> PurgeAsync();

        void DropTable();

        string Identifier { get; }
    }
}
