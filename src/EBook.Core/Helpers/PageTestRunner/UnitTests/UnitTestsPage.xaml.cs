﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace EBook.Core.Helpers.PageTestRunner.UnitTests
{
    public partial class UnitTestsPage : ContentPage
    {
        public UnitTestsPage()
        {
            InitializeComponent();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            try
            {
                var list = sender as ListView;
                UnitTestScenariosItemViewModel vm = list.SelectedItem as UnitTestScenariosItemViewModel;
                vm.ViewPageCommand.Execute(null);
                list.SelectedItem = null;


            }
            catch (Exception ex)
            {
                //ErrorManager.DisplayErrorMessage(this, ex);

            }

        }
    }
}
