using System;
using Xamarin.Forms;
using EBook.Droid;
using Android.Media;
using EBook.Abstractions;

[assembly: Dependency(typeof(AudioService))]

namespace EBook.Droid
{
   public class AudioService : IAudio
    {
        public AudioService() { }

        private MediaPlayer _mediaPlayer;

        public void Success()
        {
            _mediaPlayer = MediaPlayer.Create(global::Android.App.Application.Context, Resource.Drawable.applause);
            _mediaPlayer.Start();
        }

        public void Failure()
        {
            _mediaPlayer = MediaPlayer.Create(global::Android.App.Application.Context, Resource.Drawable.failure);
            _mediaPlayer.Start();
        }
    }
}