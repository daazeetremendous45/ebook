﻿using EBook.DataStore.Abstractions;
using EBook.DataStore.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Mock
{
    public class QuestionStore : BaseStore<Question>, IQuestionStore
    {

        public List<Question> Questions = new List<Question>();

        public QuestionStore()
        {
            SetUpQuestions();
        }

        public async override Task<Question> GetItemAsync(string id, bool forceRefresh = false)
        {
            Question retVal = new Question();
            retVal = Questions.FirstOrDefault(i => i.Id == id);
            return await Task.FromResult(retVal);
        }


        public async override Task<IEnumerable<Question>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(Questions.AsEnumerable());
        }

        public async Task<Question> GetNextQuestion(int subjectID, int sn)
        {
            return await Task.FromResult(Questions.Where(c => c.SubjectID == subjectID && c.QuestionNo == sn).FirstOrDefault());
        }

        private void SetUpQuestions()
        {
            Questions = new List<Question>()
            {
                new Question() {Id ="1", QuestionNo=1, SubjectID=1, Text="What is in the boat?",  Option1="candy", Option2="milk", Option3="pizza box", Answer="pizza box"},
                new Question() {Id ="2", QuestionNo=2, SubjectID=1, Text="What is in the boat?",  Option1="football", Option2="baseball", Option3="beach ball", Answer="football"},
                new Question() {Id ="3", QuestionNo=3, SubjectID=1, Text="What is in the boat?",  Option1="frog", Option2="turtle", Option3="snail", Answer="frog"},
                new Question() {Id ="4", QuestionNo=4, SubjectID=1, Text="What is in the boat?",  Option1="chef", Option2="doctor", Option3="dog", Answer="chef" },
                new Question() {Id ="5", QuestionNo=5, SubjectID=1, Text="What is in the boat?",  Option1="cherry", Option2="banana", Option3="apple", Answer="apple" },
                new Question() {Id ="6", QuestionNo=6, SubjectID=1, Text="What is in the boat?",  Option1="grill", Option2="micro wave", Option3="fridge", Answer="grill" },
                new Question() {Id ="7", QuestionNo=7, SubjectID=1, Text="What is in the boat?",  Option1="baby", Option2="family", Option3="grandma", Answer="grandma" },
                new Question() {Id ="8", QuestionNo=8, SubjectID=1, Text="What is in the boat?",  Option1="clock", Option2="pencil", Option3="bed", Answer="clock" },
                new Question() {Id ="9", QuestionNo=9, SubjectID=1, Text="What is in the boat?",  Option1="candies", Option2="balloons", Option3="trees", Answer="balloons" },
                new Question() {Id ="10",QuestionNo=10,SubjectID=1, Text="What is in the boat?",  Option1="contruction worker", Option2="police officer", Option3="dentist", Answer="police officer" }

            };
        }
    }
}
