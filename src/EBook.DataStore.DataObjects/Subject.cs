﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.DataObjects
{
    //[JsonObject(Title = "Subject")]
    //[ImplementPropertyChanged]
    public class Subject : BaseDataObject
    {
        public string Name { get; set; }
    }
}
