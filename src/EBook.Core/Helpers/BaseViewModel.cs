﻿using PropertyChanged;
using FreshMvvm;

namespace EBook.Core
{
    [ImplementPropertyChanged]
    public class BaseViewModel : FreshBasePageModel
    {
        public string Title { get; set; }

        public bool IsBusy { get; set; }

        public bool IsNotBusy { get; set; }

        public BaseViewModel()
        {
            IsNotBusy = true;
        }
    }
}