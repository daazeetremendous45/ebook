using System.Reflection;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using EBook.Abstractions;
using FreshMvvm;
using EBook.Core.Pages._Common.ComingSoon;
using EBook.Core.Pages.Question;
using EBook.DataStore.Abstractions;

//[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace EBook.Core
{
    public partial class App : Application, IApp
    {
        public App()
        {
            InitializeComponent();
            RegisterServices();
            StartApp();
        }

        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {

        }

        public void StartApp()
        {
            Page page;
            //remove this later
            AppSettings AppSettings = FreshIOC.Container.Resolve<IAppSettings>() as AppSettings;
            AppSettings.CurrentSubjectId = 1;
            if (EnvironmentConfiguration.Instance.IsInScreenUnitTestingMode)
            {
                page = FreshPageModelResolver.ResolvePageModel<UnitTestsViewModel>();
            }
            else
            {
                //page = FreshPageModelResolver.ResolvePageModel<ComingSoonViewModel>();
              page = FreshPageModelResolver.ResolvePageModel<QuestionViewModel>();

            }

            var container = new FreshNavigationContainer(page);
            container.BarTextColor = Color.White;
            MainPage = container;
        }
        
        private void StartFlow(Page page)
        {
            var container = new FreshMvvm.FreshNavigationContainer(page);
            container.BarTextColor = Color.White;
            MainPage = container;

            //if (EnvironmentConfiguration.Instance.IsInScreenUnitTestingMode)
            //{
            //    MainPage = container;
            //}
            //else
            //{
            //    var menu = FreshIOC.Container.Resolve<IFreshNavigationService>(CustomNav.Name) as MasterDetailPage;

            //    menu.Detail = container;
            //    menu.IsPresented = false;
            //}
        }
        public void RegisterServices()
        {
            FreshIOC.Container.Register<IApp>(this);
            if (EnvironmentConfiguration.Instance.UsesMockData)
            {
                FreshIOC.Container.Register<ISubjectStore, EBook.DataStore.Mock.SubjectStore>();
                FreshIOC.Container.Register<IQuestionStore, EBook.DataStore.Mock.QuestionStore>();
            }
            else
            {
                FreshIOC.Container.Register<ISubjectStore, EBook.DataStore.Mock.SubjectStore>();
                FreshIOC.Container.Register<IQuestionStore, EBook.DataStore.Mock.QuestionStore>();
            }



            FreshIOC.Container.Register<IAppSettings, AppSettings>();
            FreshIOC.Container.Register<IErrorManager, ErrorManager>();
            FreshIOC.Container.Register<ISession, SessionService>();
            FreshIOC.Container.Register<Acr.UserDialogs.IUserDialogs>(Acr.UserDialogs.UserDialogs.Instance);
            // FreshIOC.Container.Register(Plugin.Connectivity.CrossConnectivity.Current);

            //var assembly = typeof(App).GetTypeInfo().Assembly;
            //XamSvg.Shared.Config.ResourceAssembly = assembly;
        }

        public void Logout()
        {

        }
        
    }
}
