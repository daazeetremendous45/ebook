﻿using EBook.DataStore.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBook.DataStore.Mock
{
    public class BaseStore<T> : IBaseStore<T>
    {
        public BaseStore()
        {
        }

        public string Identifier => "store";
        public IStoreManager Manager { get; set; }

        public void DropTable()
        {
        }

        public virtual Task<T> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<T> GetItemAsync(string id, bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public virtual Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public virtual Task InitializeStore()
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> InsertAsync(T item)
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> InsertOnlineAsync(T item)
        {
            return Task.FromResult(true);
        }

        public Task<bool> PurgeAsync()
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> RemoveAsync(T item)
        {
            return Task.FromResult(true);
        }

        public Task<bool> RemoveOnlineAsync(T item)
        {
            return Task.FromResult(true);
        }

        public virtual Task<bool> SyncAsync()
        {
            return Task.FromResult(true);
        }

        public Task<bool> SyncAsync(bool pushPendingTransactions = true)
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> UpdateAsync(T item)
        {
            return Task.FromResult(true);
        }

        public Task<bool> UpdateOnlineAsync(T item)
        {
            return Task.FromResult(true);
        }
    }
}
